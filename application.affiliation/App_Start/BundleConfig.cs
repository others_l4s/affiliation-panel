﻿using System.Web;
using System.Web.Optimization;

namespace application.affiliation
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                "~/Content/vendors/js/material-vendors.min.js",
                "~/Content/vendors/js/ui/jquery.sticky.js",
                "~/Content/vendors/js/forms/icheck/icheck.min.js",
                "~/Scripts/jquery-3.5.1.min.js",
                "~/scripts/jquery.unobtrusive-ajax.min.js",
                "~/Content/js/core/app-menu.js",
                "~/Content/js/core/app.js",
                "~/Content/js/scripts/pages/material-app.js",
                "~/Content/js/toastr.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/vendors/css/material-vendors.min.css",
                      "~/Content/vendors/css/weather-icons/climacons.min.css",
                      "~/Content/vendors/fonts/meteocons/style.css",
                      "~/Content/vendors/css/forms/icheck/icheck.css",
                      "~/Content/vendors/css/forms/icheck/custom.css",
                      "~/Content/css/material.css",
                      "~/Content/css/components.css",
                      "~/Content/css/bootstrap-extended.css",
                      "~/Content/css/material-exteded.css",
                      "~/Content/css/material-colors.css",
                      "~/Content/css/colors.css",
                      "~/Content/css/core/menu/menu-types/material-horizontal-menu.css",
                      "~/Content/fonts/simple-line-icons/style.css",
                      "~/Content/css/core/colors/palette-gradient.css",
                      "~/Content/css/style.css",
                      "~/Content/css/plugins/extensions/toastr.css"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include("~/Content/vendors/js/tables/datatable/datatables.min.js",
                     "~/Content/js/scripts/tables/datatables/datatable-basic.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatablescss").Include("~/Content/vendors/css/tables/datatable/datatables.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
        }
    }
}
