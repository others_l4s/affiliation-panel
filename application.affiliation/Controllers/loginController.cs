﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.affiliation.BE;
using application.affiliation.Models;

namespace application.affiliation.Controllers
{
    public class loginController : Controller
    {
        // GET: login
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult validateUserCredentials(clsAdmin objAdmin)
        {

            AffiliationPanelEntities db = new AffiliationPanelEntities();
            if (objAdmin.UserName == ConfigurationManager.AppSettings["userName"] && objAdmin.Password == ConfigurationManager.AppSettings["password"])
            {
                string encryptedUserId = Helper.protectString(ConfigurationManager.AppSettings["userName"]);
                Helper.createEncryptedCookie(Helper.userAccessToken, encryptedUserId, true);
                return Json(new { success = true, redirectURL = Url.Content("~/formData") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "1|Invalid Username or Password" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult userLogout()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                if (Helper.doesCookieExist(Helper.userAccessToken))
                {
                    Helper.clearCookie(Helper.userAccessToken);
                }
            }
            return RedirectToAction("index", "login");
        }
    }
}