﻿using application.affiliation.BE;
using application.affiliation.Models;
using application.affiliation.VM;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.affiliation.Controllers
{
    public class formDataController : Controller
    {
        // GET: formData
        public ActionResult Index()
        {
            formDataVM objFormDataVM = new formDataVM();
            objFormDataVM.objUserDetailList = new List<UserDetail>();
            return View("index", objFormDataVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult searchMobileNo(formDataVM objFormDataVM)
        {
            AffiliationPanelEntities db = new AffiliationPanelEntities();
            formDataVM objFormDataResponseVM = new formDataVM();
            string filePath = string.Empty;
            if (objFormDataVM.postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(objFormDataVM.postedFile.FileName);
                string extension = Path.GetExtension(objFormDataVM.postedFile.FileName);
                objFormDataVM.postedFile.SaveAs(filePath);

                //Read the contents of CSV file.
                string csvData = System.IO.File.ReadAllText(filePath).Replace("\r\n", ",").Replace("MobileNo", "");
                string[] arrCSVData = csvData.Split(',');
                arrCSVData = arrCSVData.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                Nullable<DateTime> dFromDate = String.IsNullOrEmpty(objFormDataVM.FromDate) ? (DateTime?)null : new DateTime(Convert.ToInt32(objFormDataVM.FromDate.Split('-')[2]), Convert.ToInt32(objFormDataVM.FromDate.Split('-')[1]), Convert.ToInt32(objFormDataVM.FromDate.Split('-')[0]), 0, 0, 0);
                Nullable<DateTime> dToDate = String.IsNullOrEmpty(objFormDataVM.ToDate) ? (DateTime?)null : new DateTime(Convert.ToInt32(objFormDataVM.ToDate.Split('-')[2]), Convert.ToInt32(objFormDataVM.ToDate.Split('-')[1]), Convert.ToInt32(objFormDataVM.ToDate.Split('-')[0]), 23, 0, 0);
                if (dFromDate != null && dToDate != null)
                    objFormDataResponseVM.objUserDetailList = db.UserDetail.Where(x => arrCSVData.Contains(x.MobileNo.ToString()) && (x.CreatedDate >= dFromDate && x.CreatedDate <= dToDate)).ToList();
                else
                    objFormDataResponseVM.objUserDetailList = db.UserDetail.Where(x => arrCSVData.Contains(x.MobileNo.ToString())).ToList();
                objFormDataResponseVM.filterRecords = objFormDataResponseVM.objUserDetailList.Count();
                objFormDataResponseVM.totalRecords = db.UserDetail.Count();
                return View("index", objFormDataResponseVM);
            }
            else
            {
                TempData[Helper.responseStatus] = "1|Please select and upload csv file";
                return View("index", objFormDataResponseVM);
            }
        }
    }
}
