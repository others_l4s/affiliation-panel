﻿using application.affiliation.BE;
using application.affiliation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.affiliation.VM
{
    public class formDataVM
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public HttpPostedFileBase postedFile { get; set; }
        public IEnumerable<UserDetail> objUserDetailList { get; set; }
        public long totalRecords { get; set; }
        public long filterRecords { get; set; }
    }
}