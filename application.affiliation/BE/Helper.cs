﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Security.Cryptography;

namespace application.affiliation.BE
{
    public static class Helper
    {
        public const string defaultActiveStatus = "Active";
        public const string recordSaved = "0|Record Added Successfully.";
        public const string recordUpdated = "0|Record Updated Successfully.";
        public const string recordAlreadyExists = "1|Record already exists or something went wrong.";
        public const string recordDeleted = "0|Record successfully deleted.";
        public const string responseStatus = "responseStatus";
        public const string regAlphaNumericOnly = "^[a-zA-Z0-9 ]*$";
        public const string regLoginNameOnly = "^[a-zA-Z0-9.]*$";
        public const string regPassword = @"^[a-zA-Z0-9.!@#]*$";
        public const string regAlphaNumericSpecialSymbols = @"^[a-zA-Z0-9.~!@#]*$";
        public const string regAlphabetOnly = "^[a-zA-Z. ]*$";
        public const string regEmailId = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        public const string regNumericOnly = "^[0-9]*$";
        public const string regDecimalOnly = @"\d+(\.\d{1,2})?";
        public const string regWebsiteAddress = @"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?";
        public const string regGSTNo = @"^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$";
        public const string userAccessToken = "__userAccessTokenDG";
        public const string userName = "__userNameDG";

        public static DateTime getIndianTime()
        {
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return indianTime;
        }

        public static string protectString(string value2Protect)
        {
            byte[] byteString = Encoding.UTF8.GetBytes(value2Protect);
            byte[] protectedString = MachineKey.Protect(byteString, "ProtectString");
            return Convert.ToBase64String(protectedString);
        }

        public static string unProtectString(string value2UnProtect)
        {
            byte[] byteString = Convert.FromBase64String(value2UnProtect);
            byte[] protectedString = MachineKey.Unprotect(byteString, "ProtectString");
            return Encoding.UTF8.GetString(protectedString);
        }

        public static String GetRandomPasswordUsingGUID(int length)
        {
            //Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();

            //Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);

            //Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
            {
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
            }

            //Return the first length bytes
            return guidResult.Substring(0, length);
        }

        public static bool doesCookieExist(string cookieName)
        {
            return HttpContext.Current.Request.Cookies[cookieName] == null ? false : true;
        }

        public static string getCookieValueIfItExists(string cookieName)
        {
            return doesCookieExist(cookieName) ? decryptAndRetrieveCookieValue(cookieName) : null;
        }

        public static string getCookieValueIsItExists4Plain(string cookieName)
        {
            return (doesCookieExist(cookieName) ? HttpContext.Current.Request.Cookies[cookieName].Value : "");
        }

        // Creating Cookie by adding expiry of applicable.
        public static void createCookie(string cookieName, string cookieValue, bool addExpiry)
        {
            System.Web.HttpCookie cookie = new System.Web.HttpCookie(cookieName);
            cookie.Value = cookieValue;
            if (addExpiry)
            {
                cookie.Expires = System.DateTime.Today.AddDays(1);
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        // Creating Cookie by adding expiry of applicable.
        public static string createEncryptedCookie(string cookieName, string cookieValue, bool addExpiry)
        {
            System.Web.HttpCookie cookie = new System.Web.HttpCookie(cookieName);
            cookie.Value = protectString(cookieValue);
            if (addExpiry)
            {
                cookie.Expires = System.DateTime.Today.AddDays(1);
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie.Value;
        }

        public static string decryptAndRetrieveCookieValue(string cookieName)
        {
            if (doesCookieExist(cookieName))
                return unProtectString(System.Convert.ToString(HttpContext.Current.Request.Cookies[cookieName].Value));
            else
                return null;
        }
        public static void clearCookie(string cookieName)
        {
            if (HttpContext.Current.Request.Cookies[cookieName] != null)
            {
                HttpContext.Current.Response.Cookies[cookieName].Value = "";
                HttpContext.Current.Response.Cookies[cookieName].Expires = DateTime.Now.AddYears(-1);
            }
        }

        public static Int32 decryptCookieAndRetriveUnProtectedCookieValue(string cookieName)
        {
            string unProtectedValue = Helper.decryptAndRetrieveCookieValue(cookieName);
            if (!string.IsNullOrEmpty(unProtectedValue))
                return Convert.ToInt32(Helper.unProtectString(unProtectedValue));
            else
                return 0;
        }

        public static String GetRFC822Date_format1(DateTime date)
        {
            int offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours;
            string timeZone = "+" + offset.ToString().PadLeft(2, '0');

            if (offset < 0)
            {
                int i = offset * -1;
                timeZone = "-" + i.ToString().PadLeft(2, '0');
            }
            return date.ToString("dd MMM yyyy HH:mm tt");
        }

        public static string EncryptString(string plainText, byte[] key)
        {
            // Instantiate a new Aes object to perform string symmetric encryption
            Aes encryptor = Aes.Create();
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
            encryptor.Mode = CipherMode.CBC;

            // Set key and IV
            byte[] aesKey = new byte[32];
            Array.Copy(key, 0, aesKey, 0, 32);
            encryptor.Key = aesKey;
            encryptor.IV = iv;

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Instantiate a new encryptor from our Aes object
            ICryptoTransform aesEncryptor = encryptor.CreateEncryptor();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesEncryptor, CryptoStreamMode.Write);

            // Convert the plainText string into a byte array
            byte[] plainBytes = Encoding.ASCII.GetBytes(plainText);

            // Encrypt the input plaintext string
            cryptoStream.Write(plainBytes, 0, plainBytes.Length);

            // Complete the encryption process
            cryptoStream.FlushFinalBlock();

            // Convert the encrypted data from a MemoryStream to a byte array
            byte[] cipherBytes = memoryStream.ToArray();

            // Close both the MemoryStream and the CryptoStream
            memoryStream.Close();
            cryptoStream.Close();

            // Convert the encrypted byte array to a base64 encoded string
            string cipherText = Convert.ToBase64String(cipherBytes, 0, cipherBytes.Length);

            // Return the encrypted data as a string
            return cipherText;
        }

        public static string DecryptString(string cipherText, byte[] key)
        {
            // Instantiate a new Aes object to perform string symmetric encryption
            Aes encryptor = Aes.Create();

            encryptor.Mode = CipherMode.CBC;
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
            // Set key and IV
            byte[] aesKey = new byte[32];
            Array.Copy(key, 0, aesKey, 0, 32);
            encryptor.Key = aesKey;
            encryptor.IV = iv;

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Instantiate a new encryptor from our Aes object
            ICryptoTransform aesDecryptor = encryptor.CreateDecryptor();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesDecryptor, CryptoStreamMode.Write);

            // Will contain decrypted plaintext
            string plainText = String.Empty;

            try
            {
                // Convert the ciphertext string into a byte array
                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                // Decrypt the input ciphertext string
                cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);

                // Complete the decryption process
                cryptoStream.FlushFinalBlock();

                // Convert the decrypted data from a MemoryStream to a byte array
                byte[] plainBytes = memoryStream.ToArray();

                // Convert the decrypted byte array to string
                plainText = Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length);
            }
            finally
            {
                // Close both the MemoryStream and the CryptoStream
                memoryStream.Close();
                cryptoStream.Close();
            }

            // Return the decrypted data as a string
            return plainText;
        }

        public static byte[] create_SHA256Hash()
        {
            string key = "3sc3RLrpd17";
            SHA256 mySHA256 = SHA256Managed.Create();
            return mySHA256.ComputeHash(Encoding.ASCII.GetBytes(key));
        }
    }
}