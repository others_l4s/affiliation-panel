﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.affiliation.BE
{
    public class clsAdmin
    {
        [Required(ErrorMessage = "Please enter Username")]
        [StringLength(100, ErrorMessage = "Please enter valid Username")]
        [Display(Name = "Enter UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [StringLength(20, ErrorMessage = "Please enter valid Password (Minimum length is 4 and Max length is 20", MinimumLength = 4)]
        [Display(Name = "Enter Password")]
        public string Password { get; set; }
    }
}